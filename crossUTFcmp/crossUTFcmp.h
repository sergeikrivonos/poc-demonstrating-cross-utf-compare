#pragma once
#include <string_view>

bool crossUTFequal(std::u8string_view utf8, std::u16string_view utf16);
