#define BOOST_TEST_MODULE measureCrossUTFcmp test
#include "crossUTFcmp.h"
#include <boost/test/unit_test.hpp>
#include <omnn/rt/diag.hpp>
#include <codecvt>
#include <locale>

namespace{
    std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> utf8to16;
    std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> utf16to8;
}

BOOST_AUTO_TEST_CASE(measure){
    using namespace std::string_literals;
    std::u8string s8 = u8"ɦΈ˪˪ʘ";
    std::u8string_view utf8(s8);
    std::u16string s = u"ɦΈ˪˪ʘ";
    std::u16string_view utf16(s);
    auto crosscmp = crossUTFequal(utf8, utf16);
    BOOST_TEST(crosscmp);

    auto time = omnn::measure::time([&]{
        for(auto i = 0; i < 1000000; i++){
            crosscmp = crossUTFequal(utf8, utf16);
        }
    });
    std::cout << "cross cmp time: " << time.count() << std::endl;


    // UTF-8 to UTF-16/char16_t
    auto convcmp = utf8to16.from_bytes((char*)s8.c_str()) == utf16;
    BOOST_TEST(convcmp);

    time = omnn::measure::time([&]{
        for(auto i = 0; i < 1000000; i++){
            convcmp = utf8to16.from_bytes((char*)s8.c_str()) == utf16;
        }
    });
    std::cout << "cmp with UTF8->UTF16 time: " << time.count() << std::endl;


    // UTF-16/char16_t to UTF-8
    std::string u8_conv = utf16to8.to_bytes(s.c_str());
    convcmp = std::string_view(u8_conv) == *reinterpret_cast<std::string_view*>(&utf8);
    BOOST_TEST(convcmp);
    time = omnn::measure::time([&]{
        for(auto i = 0; i < 1000000; i++){
            convcmp = *reinterpret_cast<std::string_view*>(&utf8) == std::string_view(utf16to8.to_bytes(s.c_str()));
        }
    });
    std::cout << "cmp with UTF16->UTF8 time: " << time.count() << std::endl;

}
