#include "crossUTFcmp.h"

namespace
{
const char trailingBytesForUTF8[256] = {
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
    2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2, 3,3,3,3,3,3,3,3,4,4,4,4,5,5,5,5
};

const char32_t offsetsFromUTF8[6] = { 0x00000000UL, 0x00003080UL, 0x000E2080UL, 
                     0x03C82080UL, 0xFA082080UL, 0x82082080UL };
}

char32_t readNextChar(std::u16string_view::iterator& it){
    constexpr char32_t UNI_SUR_HIGH_START = 0xD800;
    constexpr char32_t UNI_SUR_HIGH_END = 0xDBFF;
    constexpr char32_t UNI_SUR_LOW_START = 0xDC00;
    constexpr char32_t UNI_SUR_LOW_END = 0xDFFF;
    constexpr char32_t halfBase = 0x0010000UL;
    constexpr int halfShift  = 10;
    char32_t ch = *it;
    ++it;
    if(ch >= UNI_SUR_HIGH_START && ch <= UNI_SUR_HIGH_END){
        auto ch2 = *it;
        if (ch2 >= UNI_SUR_LOW_START && ch2 <= UNI_SUR_LOW_END) {
            ch = ((ch - UNI_SUR_HIGH_START) << halfShift)
                + (ch2 - UNI_SUR_LOW_START) + halfBase;
            ++it;
        }
    }
    return ch;
}

char32_t readNextChar(std::u8string_view::iterator& it){
    char32_t ch = 0;
    auto extraBytesToRead = trailingBytesForUTF8[*it];
    switch (extraBytesToRead) {
    case 5: ch += *it++; ch <<= 6;
    case 4: ch += *it++; ch <<= 6;
    case 3: ch += *it++; ch <<= 6;
    case 2: ch += *it++; ch <<= 6;
    case 1: ch += *it++; ch <<= 6;
    case 0: ch += *it++;
    }
    ch -= offsetsFromUTF8[extraBytesToRead];
    return ch;
}

bool crossUTFequal(std::u8string_view utf8, std::u16string_view utf16) {
    auto it8 = utf8.begin(), e8 = utf8.end();
    auto it16 = utf16.begin(), e16 = utf16.end();
    
    while(it8 != e8 && it16 != e16)
    {
        if(readNextChar(it8) != readNextChar(it16))
            return false;
    }

    return true;
}
